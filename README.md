Installing
=============
In Julia do the following:

    julia> Pkg.clone("git@bitbucket.org:troelsfrostholm/physicsconstants.git")

Usage
=============
This package provides the module PhysicsConstants, which contains the submodule CGS with a random selection of physics constants. 

So for example:

    julia> import PhysicsConstants.CGS
    julia> CGS.G       # Newtons gravitational constant in cm^3 g^-1 s^-2
    6.67259e-8

or

    julia> using PhysicsConstants.CGS
    julia> G       # Newtons gravitational constant in cm^3 g^-1 s^-2
    6.67259e-8

Inspect the module contents with

    julia> whos(PhysicsConstants.CGS)

or

    julia> names(PhysicsConstants.CGS)
module PhysicsConstants
  module CGS
    export G, AU, M_sol, k_b, yr, kyr, myr, ev, m_p, u
    G = 6.67259e-8         # Newtons gravitational constant in cm^3 g^-1 s^-2
    AU = 1.49597871e13     # 1 astronomical unit in cm
    M_sol = 1.9891e33      # one solar mass in grams
    k_b = 1.3806488e-16    # Boltzmann constant in cm^2 g s^-2 K^-1
    u = 1.660539040e-24    # Atomic mass unit in grams
    m_p = 1.6726219e-24    # Proton mass in grams
    yr = 31556926          # 1 year in seconds
    kyr = 1e3yr            # Kiloyear in seconds
    myr = 1e6yr            # Megayear in seconds
    ev = 1.60217662e-12    # Electron volt in erg
  end
end